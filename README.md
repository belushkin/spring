# Mini bank Java application assessment

The goal of the application is to demonstrate obtained skills on Java Spring boot framework.

## Goal
Create mini bank application with customers and clients.
Application must allow to:
- Add/Remove customers
- Add/Remove accounts to customers with different currencies
- Replenish/Withdraw assets from the accounts
- Transfer assets from one account to another

## Technologies used
- Java 8
- JavaScript
- Spring boot
- React + MaterialUI
- AWS ECS (Docker)
- Tomcat
- Maven
- Junit4

## URL
- http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com/

### How app does look like

![Login menu](./assets/img/image1.png)

![Users list](./assets/img/image2.png)

![Liked profiles](./assets/img/image3.png)

### Run the app
- docker-compose up

### ECS commands for reference
- docker tag springbootreact_app-client:latest 030910990668.dkr.ecr.us-east-1.amazonaws.com/springbootreact_app-client:1
- docker push 030910990668.dkr.ecr.us-east-1.amazonaws.com/springbootreact_app-client:1
- docker push 030910990668.dkr.ecr.us-east-1.amazonaws.com/springbootreact_app-server:latest
- docker ecs compose ps
- docker ecs compose up
- docker context use aws

## Contributors
- @belushkin
