import React, { Component } from 'react';
import axios from 'axios'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const styles = (theme) => ({
    root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(8),
          width: '25ch',
        },
      },
  });

class CustomersEdit extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: 0,
			name: '',
			email: '',
			age: 0
		}
    }

    componentDidMount() {
        console.log(this.props.match);
        let id = this.props.match.params.id;

        axios.get(`http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/customers/${id}`)
        .then(response => {
            console.log(response)
            this.setState(response.data);
        })
        .catch(error => {
            console.log(error)
        })
    }

	changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
    }

    breadCrumbsHandler = e => {
        e.preventDefault();
        this.props.history.push('/');
    }
    
    submitHandler = e => {
		e.preventDefault()
		// console.log(this.state)
		axios
			.put('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/customers/' + this.state.id, this.state)
			.then(response => {
                console.log(response)
                this.props.history.push('/');
                this.props.getCustomers()
			})
			.catch(error => {
				console.log(error)
			})
    }

    render() {
        const { classes } = this.props;
        const { name, email, age } = this.state

        return (
            <div>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" onClick={this.breadCrumbsHandler}>
                        Dashboard
                    </Link>
                    <Typography color="textPrimary">Edit customer</Typography>
                </Breadcrumbs>
                <form className={classes.root} noValidate autoComplete="off" onSubmit={this.submitHandler}>
                    <TextField 
                        id="customer-name"
                        label="Customer name" 
                        fullWidth
                        value={name}
                        name="name"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <TextField 
                        id="customer-email"
                        label="Customer email" 
                        fullWidth
                        value={email}
                        name="email"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <TextField 
                        id="customer-age"
                        label="Customer age" 
                        fullWidth
                        value={age}
                        name="age"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <div>
                        <Button variant="contained" color="primary" type="submit">
                            Submit
                        </Button>
                    </div>
                </form>
            </div>
            
        );
    }
}

export default withStyles(styles)(CustomersEdit);
