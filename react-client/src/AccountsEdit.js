import React, { Component } from 'react';
import axios from 'axios'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const styles = (theme) => ({
    root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(6),
          width: '25ch',
        },
      },
  });

class AccountsEdit extends Component {

    constructor(props) {
        super(props);

        this.state = {
			number: '',
			currency: '',
            balance: 0,
            currentbalance: 0
		}
    }

    componentDidMount() {
        console.log(this.props.match);
        let number = this.props.match.params.number;

        axios.get(`http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/accounts`)
        .then(response => {
            // console.log(response)
            let filtered = response.data.filter(function(account) {
                return account.number === number;
            });
            // console.log(filtered);
            if (filtered.length) {
                this.setState(filtered[0]);
            }
        })
        .catch(error => {
            console.log(error)
        })
    }

    breadCrumbsHandler = e => {
        e.preventDefault();
        this.props.history.push('/');
    }

	changeHandler = e => {
        // console.log(e.target.name)
        // console.log(e.target.value)
		this.setState({ [e.target.name]: e.target.value })
    }

    submitHandler = e => {
		e.preventDefault()
        // console.log(this.state);
        if (this.state.currentbalance > 0) {
		axios
			.put('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/accounts/'+this.state.number+'/replenish/'+this.state.currentbalance)
			.then(response => {
                // console.log(response)
                this.props.history.push('/');
                this.props.getCustomers()
			})
			.catch(error => {
				console.log(error)
			})
        } else if (this.state.currentbalance < 0) {
            axios
			.put('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/accounts/'+this.state.number+'/withdraw/'+(-this.state.currentbalance))
			.then(response => {
                // console.log(response)
                this.props.history.push('/');
                this.props.getCustomers()
			})
			.catch(error => {
				console.log(error)
			})
        } else {
            this.props.history.push('/');
            this.props.getCustomers()
        }
    }

    render() {
        const { classes } = this.props;
        const { number, currency, balance, currentbalance } = this.state

        return (
            <div>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" onClick={this.breadCrumbsHandler}>
                        Dashboard
                    </Link>
                    <Typography color="textPrimary">Update account</Typography>
                </Breadcrumbs>
                <form className={classes.root} noValidate autoComplete="off" onSubmit={this.submitHandler}>
                    <TextField 
                        id="account-number"
                        label="Account number" 
                        fullWidth
                        InputProps={{
                            readOnly: true,
                        }}
                        value={number}
                        name="number"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <TextField 
                        id="account-currency"
                        label="Account currency" 
                        fullWidth
                        InputProps={{
                            readOnly: true,
                        }}
                        value={currency}
                        name="currency"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <TextField 
                        id="account-current-balance"
                        label="Account current balance" 
                        fullWidth
                        InputProps={{
                            readOnly: true,
                        }}
                        value={balance}
                        name="balance"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <TextField 
                        id="account-new-balance"
                        label="Add value to your balance" 
                        fullWidth
                        value={currentbalance}
                        name="currentbalance"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <div>
                        <Button variant="contained" color="primary" type="submit">
                            Submit
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

export default withStyles(styles)(AccountsEdit);
