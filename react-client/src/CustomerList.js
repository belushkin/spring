import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Orders from './Orders';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import {Link as RouterLink} from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
}));

const CustomerList = ({customers, deleteAccount, deleteCustomer}) => {
    const classes = useStyles();
    const [dialog, setDialog] = React.useState(false);
    const handleClickOpen = () => {
        setDialog(true);
    };
    
    const handleClose = () => {
        setDialog(false);
    };

    const deleteHandler = (id) => {
        setDialog(false);
        deleteCustomer(id)();
    };
    return (
        <Grid container spacing={3}>
            {customers.map((customer) => (
                <Grid key={customer.id} item xs={12}>
                    <Typography align="left" variant="h5" type="title" color="inherit">
                        {customer.name} | {customer.email} | {customer.age} years
                        <Tooltip title="Edit current customer">
                            <IconButton component={RouterLink} to={`/customers/${customer.id}`}>
                                <EditIcon color="primary"/>
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Add account to current customer">
                            <IconButton component={RouterLink} to={`/customers/${customer.id}/new`}>
                                <AddIcon color="primary"/>
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Delete current account">
                            <IconButton onClick={handleClickOpen}>
                                <DeleteIcon color="secondary"/>
                            </IconButton>
                        </Tooltip>
                        <Dialog
                            open={dialog}
                            onClose={handleClose}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogTitle id="alert-dialog-title">{"Delete this customer?"}</DialogTitle>
                            <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                By deleting this customer you delete all his accounts as well and you will never undo this action!
                            </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                            <Button onClick={handleClose} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={() => deleteHandler(customer.id)} color="primary" autoFocus>
                                Delete
                            </Button>
                            </DialogActions>
                        </Dialog>
                    </Typography>
                    {customer.accounts.length ? (
                        <Paper className={classes.paper}>
                            <Orders 
                                customer={customer}
                                accounts={customer.accounts}
                                deleteAccount = {deleteAccount}
                            />
                        </Paper>
                    ) : (
                        <Typography variant="body2">No accounts</Typography>
                    )}
                </Grid>
            ))}
        </Grid>
    );
};

export default CustomerList;