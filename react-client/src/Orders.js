import React, { useEffect } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import {Link as RouterLink} from 'react-router-dom';
import Title from './Title'
import Tooltip from '@material-ui/core/Tooltip';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function Orders(props) {
    const {customer, accounts, deleteAccount} = props
    const [dialog, setDialog] = React.useState(false);

    const handleClickOpen = () => {
        setDialog(true);
    };
    
    const handleClose = () => {
        setDialog(false);
    };

    const deleteHandler = (id, number) => {
        setDialog(false);
        deleteAccount(id, number)();
    };

    return (
        <React.Fragment>
            <Title>Accounts</Title>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Number</TableCell>
                        <TableCell>Currency</TableCell>
                        <TableCell>Balance</TableCell>
                        <TableCell>Edit</TableCell>
                        <TableCell align="right">Delete</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {accounts.map((account) => (
                        <TableRow key={account.number}>
                            <TableCell>{account.number}</TableCell>
                            <TableCell>{account.currency}</TableCell>
                            <TableCell>{account.balance}</TableCell>
                            <TableCell>
                                <Tooltip title="Edit current account">
                                    <IconButton component={RouterLink} to={`/accounts/${account.number}`}>
                                        <EditIcon />
                                    </IconButton>
                                </Tooltip>
                            </TableCell>
                            <TableCell align="right">
                                <Tooltip title="Delete current account">
                                    <IconButton onClick={handleClickOpen}>
                                        <DeleteIcon color="secondary"/>
                                    </IconButton>
                                </Tooltip>
                                <Dialog
                                    open={dialog}
                                    onClose={handleClose}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">{"Delete this account?"}</DialogTitle>
                                    <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        By deleting this account you will never undo this action!
                                    </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                    <Button onClick={handleClose} color="primary">
                                        Cancel
                                    </Button>
                                    <Button onClick={() => deleteHandler(customer.id, account.number)} color="primary" autoFocus>
                                        Delete
                                    </Button>
                                    </DialogActions>
                                </Dialog>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </React.Fragment>
    );
}