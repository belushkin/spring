import React, { Component } from 'react';
import axios from 'axios'
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const styles = (theme) => ({
    root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(6),
          width: '25ch',
        },
      },
      selectEmpty: {
        marginTop: theme.spacing(6),
      },
      formControl: {
        margin: theme.spacing(6),
        minWidth: 120,
      },
  });

class Transfer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            accounts: [],
            to: '',
            from: '',
            balance: 0
		}
    }

    componentDidMount() {
        axios.get(`http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/accounts`)
        .then(response => {
            console.log(response)
            this.setState({ accounts:response.data });
        })
        .catch(error => {
            console.log(error)
        })
    }

	changeHandler = e => {
		this.setState({ [e.target.name]: e.target.value })
    }

    breadCrumbsHandler = e => {
        e.preventDefault();
        this.props.history.push('/');
    }
    
    submitHandler = e => {
		e.preventDefault()
		// console.log(this.state)
		axios
			.put('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/accounts/'+this.state.from+'/to/'+this.state.to+'/transfer/'+this.state.balance)
			.then(response => {
                console.log(response)
                this.props.history.push('/');
                this.props.getCustomers()
			})
			.catch(error => {
				console.log(error)
			})
    }

    render() {
        const { classes } = this.props;
        const { to, from, balance } = this.state

        return (
            <div>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" onClick={this.breadCrumbsHandler}>
                        Dashboard
                    </Link>
                    <Typography color="textPrimary">Transfer assets between accounts</Typography>
                </Breadcrumbs>
                <form className={classes.root} noValidate autoComplete="off" onSubmit={this.submitHandler}>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="account-number-from-label">Account from</InputLabel>
                        <Select
                            labelId="account-number-from-label"
                            id="account-number-from"
                            name="from"
                            value={from}
                            onChange={this.changeHandler}
                        >
                            {this.state.accounts.map((account) => (
                                <MenuItem value={account.number}>{account.number} {account.currency} {account.balance}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="account-number-to-label">Account to</InputLabel>
                        <Select
                            labelId="account-number-to-label"
                            id="account-number-to"
                            name="to"
                            value={to}
                            onChange={this.changeHandler}
                        >
                            {this.state.accounts.map((account) => (
                                <MenuItem value={account.number}>{account.number} {account.currency} {account.balance}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <TextField 
                        id="transfer-balance"
                        label="Amount to transfer" 
                        fullWidth
                        value={balance}
                        name="balance"
                        onChange={this.changeHandler}
                        variant="outlined"
                    />
                    <div>
                        <Button variant="contained" color="primary" type="submit">
                            Submit
                        </Button>
                    </div>
                </form>
            </div>
            
        );
    }
}

export default withStyles(styles)(Transfer);
