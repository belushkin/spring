import React from 'react';
import './App.css';
import Dashboard from "./Dashboard";
import { Button } from '@material-ui/core';
import Orders from "./Orders";
import Title from "./Title";
import ParentDashboard from './ParentDashboard';

function App() {
  return (
    <div className="App">
        <ParentDashboard/>
    </div>
  );
}

export default App;
