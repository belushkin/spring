import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import BarChartIcon from '@material-ui/icons/BarChart';
import AssignmentIcon from '@material-ui/icons/Assignment';
import {Link as RouterLink} from 'react-router-dom';
import AddIcon from '@material-ui/icons/Add';
import CompareArrowsIcon from '@material-ui/icons/CompareArrows';

export const mainListItems = (
    <div>
        <ListItem button component={RouterLink} to="/">
            <ListItemIcon>
                <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
        </ListItem>
        <ListItem button component={RouterLink} to="/customers">
            <ListItemIcon>
                <AddIcon />
            </ListItemIcon>
            <ListItemText primary="Add new customer" />
        </ListItem>
        <ListItem button component={RouterLink} to="/transfer">
            <ListItemIcon>
                <CompareArrowsIcon />
            </ListItemIcon>
            <ListItemText primary="Transfer accounts" />
        </ListItem>
    </div>
);
