import React, { Component } from 'react';
import axios from 'axios'
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

const styles = (theme) => ({
    root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(1),
          width: '25ch',
        },
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
      formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
  });

class AccountsEdit extends Component {

    constructor(props) {
        super(props);

        this.state = {
			currency: ''
		}
    }

	changeHandler = e => {
        // console.log(e.target.name)
        // console.log(e.target.value)
		this.setState({ [e.target.name]: e.target.value })
    }

    breadCrumbsHandler = e => {
        e.preventDefault();
        this.props.history.push('/');
    }

    submitHandler = e => {
		e.preventDefault()
        // console.log(this.state)
        let id = this.props.match.params.id;
		axios
			.post('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/customers/'+id+'/accounts/'+this.state.currency)
			.then(response => {
                console.log(response)
                this.props.history.push('/');
                this.props.getCustomers()
			})
			.catch(error => {
				console.log(error)
			})
    }

    render() {
        const { classes } = this.props;
        const { currency } = this.state

        return (
            <div>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/" onClick={this.breadCrumbsHandler}>
                        Dashboard
                    </Link>
                    <Typography color="textPrimary">Add new account</Typography>
                </Breadcrumbs>
                <form className={classes.root} noValidate autoComplete="off" onSubmit={this.submitHandler}>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="account-currency-label">Currency</InputLabel>
                        <Select
                            labelId="account-currency-label"
                            id="account-currency"
                            value={currency}
                            name="currency"
                            onChange={this.changeHandler}
                        >
                            <MenuItem value="UAH">UAH</MenuItem>
                            <MenuItem value="EUR">EUR</MenuItem>
                            <MenuItem value="USD">USD</MenuItem>
                            <MenuItem value="CHF">CHF</MenuItem>
                            <MenuItem value="GBP">GBP</MenuItem>
                        </Select>
                    </FormControl>

                    <div>
                        <Button variant="contained" color="primary" type="submit">
                            Submit
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

export default withStyles(styles)(AccountsEdit);
