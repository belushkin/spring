import React, { Component } from 'react';
import Dashboard from './Dashboard';
import axios from 'axios';

class ParentDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            customers: [],
            errorMsg: ""
        }
    }
    
    getCustomers = e => {
		axios
			.get('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/customers')
			.then(response => {
				console.log(response)
				this.setState({ customers: response.data })
			})
			.catch(error => {
                console.log(error)
                this.setState({errorMsg: 'Error retrieving data'})
			})
    }
    
    deleteCustomersAccountHandler = (customer, account) => () => {
        console.log(customer)
        console.log(account)
        axios
        .delete('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/customers/'+customer+'/accounts/'+account)
        .then(response => {
            console.log(response)
            this.getCustomers();
        })
        .catch(error => {
            console.log(error)
        })
    }
    
    deleteCustomerHandler = (customer) => () => {
        console.log(customer)
        axios
        .delete('http://springbootreactloadbalancer-ad6bbe932bee413a.elb.us-east-1.amazonaws.com:9000/api/v1/customers/'+customer)
        .then(response => {
            console.log(response)
            this.getCustomers();
        })
        .catch(error => {
            console.log(error)
        })
    }

    componentDidMount() {
		this.getCustomers()
    }

    render() {
        return (
            <Dashboard 
                customers={this.state.customers} 
                deleteAccount = {this.deleteCustomersAccountHandler}
                deleteCustomer = {this.deleteCustomerHandler}
                getCustomers = {this.getCustomers}
            />
        );
    }
}

export default ParentDashboard;