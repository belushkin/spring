package com.app.repository;

import com.app.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepository implements DAO<Customer> {

    private List<Customer> customers = new ArrayList<>();

    public Customer save(Customer customer) {
        if (customers.size() == 0) {
            customer.setId(1);
        } else {
            customer.setId(customers.get(customers.size() - 1).getId() + 1);
        }
        if (!customers.contains(customer)) {
            customers.add(customer);
            return customer;
        }
        return null;
    }

    public List<Customer> findAll() {
        return customers;
    }

    public void saveAll(List<Customer> customers) {
        for (Customer customer : customers) {
            save(customer);
        }
    }

    public void deleteAll(List<Customer> customers) {
        for (Customer customer : customers) {
            deleteById(customer.getId());
        }
    }

    public void updateById(int id, Customer customer) {
        customer.setId(id);
        customers.set(
                customers.indexOf(getOne(id)),
                customer
        );
    }

    public boolean delete(Customer customer) {
        return customers.remove(customer);
    }

    public boolean deleteById(int id) {
        return customers.removeIf(customer -> customer.getId() == id);
    }

    public Customer getOne(int id) {
        return customers.
                stream().
                filter(customer -> customer.getId() == id).
                findAny().
                orElse(null);
    }
}
