package com.app.repository;

import com.app.model.Account;
import com.app.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AccountRepository implements DAO<Account> {

    private List<Account> accounts = new ArrayList<>();

    public Account save(Account account) {
        if (accounts.size() == 0) {
            account.setId(1);
        } else {
            account.setId(accounts.get(accounts.size() - 1).getId() + 1);
        }
        if (!accounts.contains(account)) {
            accounts.add(account);
            return account;
        }
        return null;
    }

    public List<Account> findAll() {
        return accounts;
    }

    public void saveAll(List<Account> accounts) {
        for (Account account : accounts) {
            save(account);
        }
    }

    public void deleteAll(List<Account> accounts) {
        for (Account account : accounts) {
            deleteById(account.getId());
        }
    }

    public boolean delete(Account account) {
        return accounts.remove(account);
    }

    public boolean deleteById(int id) {
        return accounts.removeIf(account -> account.getId() == id);
    }

    public Account getOne(int id) {
        return accounts.
                stream().
                filter(account -> account.getId() == id).
                findAny().
                orElse(null);
    }

    public Account getOneByNumber(String number) {
        return accounts.
                stream().
                filter(account -> account.getNumber().equals(number)).
                findAny().
                orElse(null);
    }
    public void deleteAccountsByCustomer(Customer customer) {
        accounts.removeIf(e -> e.getCustomer().equals(customer));
    }

    public void updateById(int id, Account account) {
        account.setId(id);
        accounts.set(
                accounts.indexOf(getOne(id)),
                account
        );
    }
}
