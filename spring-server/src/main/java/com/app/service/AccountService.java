package com.app.service;

import com.app.model.Account;
import com.app.model.Customer;
import com.app.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Account save(Account account) {
        return accountRepository.save(account);
    }
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public void saveAll(List<Account> accounts) {
        accountRepository.saveAll(accounts);
    }

    public void deleteAll(List<Account> accounts) {
        accountRepository.deleteAll(accounts);
    }

    public boolean delete(Account account) {
        return accountRepository.delete(account);
    }

    public boolean deleteById(int id) {
        return accountRepository.deleteById(id);
    }

    public Account getOne(int id) {
        return accountRepository.getOne(id);
    }

    public Account getOneByNumber(String number) {
        return accountRepository.getOneByNumber(number);
    }

    public void deleteAccountsByCustomer(Customer customer) {
        accountRepository.deleteAccountsByCustomer(customer);
    }

    public void updateById(int id, Account account) {
        accountRepository.updateById(id, account);
    }
}
