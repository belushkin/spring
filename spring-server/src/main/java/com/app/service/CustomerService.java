package com.app.service;

import com.app.model.Customer;
import com.app.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public void saveAll(List<Customer> customers) {
        customerRepository.saveAll(customers);
    }

    public void deleteAll(List<Customer> customers) {
        customerRepository.deleteAll(customers);
    }

    public void updateById(int id, Customer customer) {
        customerRepository.updateById(id, customer);
    }

    public boolean delete(Customer customer) {
        return customerRepository.delete(customer);
    }

    public boolean deleteById(int id) {
        return customerRepository.deleteById(id);
    }

    public Customer getOne(int id) {
        return customerRepository.getOne(id);
    }

}
