package com.app.controller;

import com.app.model.Account;
import com.app.model.Currency;
import com.app.model.Customer;
import com.app.service.AccountService;
import com.app.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AccountService accountService;

    @GetMapping(value = "/customers")
    @CrossOrigin()
    public List<Customer> findAll(HttpServletRequest request) {
        return customerService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/customers")
    @CrossOrigin()
    public void createCustomer(@RequestBody Customer customer){
        customerService.save(customer);
    }

    @PostMapping(value = "/customers/{id}/accounts/{currency}")
    @CrossOrigin()
    public void addAccountToCustomer(@PathVariable int id, @PathVariable String currency){
        Customer customer = customerService.getOne(id);
        Account account = accountService.save(
                new Account(Currency.valueOf(currency), customer)
        );

        customer.addAccount(account);
        customerService.updateById(id, customer);
    }

    @GetMapping(value = "/customers/{id}")
    @CrossOrigin()
    public Customer getCustomerById(@PathVariable int id){
        return customerService.getOne(id);
    }

    @PutMapping(value = "/customers/{id}")
    @CrossOrigin()
    public void updateCustomerById(@PathVariable int id, @RequestBody Customer customer){
        customerService.updateById(id, customer);
    }

    @DeleteMapping(value = "/customers/{id}")
    @CrossOrigin()
    public void deleteCustomerById(@PathVariable int id){
        accountService.deleteAccountsByCustomer(customerService.getOne(id));
        customerService.deleteById(id);
    }

    @DeleteMapping(value = "/customers/{id}/accounts/{number}")
    @CrossOrigin()
    public void deleteAccountOfCustomer(@PathVariable int id, @PathVariable String number){
        Customer customer = customerService.getOne(id);
        Account account = accountService.getOneByNumber(number);

        List<Account> accounts = customer.getAccounts();
        accounts.remove(account);
        customer.setAccounts(accounts);

        customerService.updateById(id, customer);
        accountService.delete(account);
    }
}
