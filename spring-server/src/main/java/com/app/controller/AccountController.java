package com.app.controller;

import com.app.model.Account;
import com.app.model.Customer;
import com.app.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping(value = "/accounts")
    @CrossOrigin()
    public List<Account> findAll(HttpServletRequest request) {
        return accountService.findAll();
    }

    @PutMapping(value = "/accounts/{number}/replenish/{balance}")
    @CrossOrigin()
    public void replenishAccount(@PathVariable String number, @PathVariable Double balance){
        Account account = accountService.getOneByNumber(number);
        account.setBalance(account.getBalance() + balance);
        accountService.updateById(account.getId(), account);
    }

    @PutMapping(value = "/accounts/{number}/withdraw/{balance}")
    @CrossOrigin()
    public void withdrawAccount(@PathVariable String number, @PathVariable Double balance){
        Account account = accountService.getOneByNumber(number);
        if (balance <= account.getBalance()) {
            account.setBalance(account.getBalance() - balance);
            accountService.updateById(account.getId(), account);
        }
    }

    @PutMapping(value = "/accounts/{numberfrom}/to/{numberto}/transfer/{balance}")
    @CrossOrigin()
    public void transferAccount(@PathVariable String numberfrom, @PathVariable String numberto, @PathVariable Double balance){
        Account accountFrom = accountService.getOneByNumber(numberfrom);
        Account accountTo = accountService.getOneByNumber(numberto);

        if (balance <= accountFrom.getBalance()) {
            accountFrom.setBalance(accountFrom.getBalance() - balance);
            accountTo.setBalance(accountTo.getBalance() + balance);
            accountService.updateById(accountFrom.getId(), accountFrom);
            accountService.updateById(accountTo.getId(), accountTo);
        }
    }
}
