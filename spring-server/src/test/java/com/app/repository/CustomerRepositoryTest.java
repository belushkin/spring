package com.app.repository;

import com.app.model.Account;
import com.app.model.Currency;
import com.app.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class CustomerRepositoryTest {
    @Test
    public void test_adding_new_customer() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        customerRepository.save(new Customer("name", "email", 22));
        // then
        Assert.assertEquals(1, customerRepository.findAll().size());
    }

    @Test
    public void test_removing_customer() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer = new Customer("name", "email", 22);
        customerRepository.save(customer);
        // then
        Assert.assertTrue(customerRepository.delete(customer));
        Assert.assertEquals(0, customerRepository.findAll().size());
    }

    @Test
    public void test_removing_customer_by_id() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer = new Customer("name", "email", 22);
        customerRepository.save(customer);
        // then
        Assert.assertFalse(customerRepository.deleteById(999));
        Assert.assertTrue(customerRepository.deleteById(1));
        Assert.assertEquals(0, customerRepository.findAll().size());
    }

    @Test
    public void test_removing_customer_by_non_existing_id() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer = new Customer("name", "email", 22);
        customerRepository.save(customer);
        // then
        Assert.assertFalse(customerRepository.deleteById(999));
        Assert.assertEquals(1, customerRepository.findAll().size());
    }

    @Test
    public void test_removing_customer_by_non_existing_customer() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer1 = new Customer("name1", "email1", 22);
        Customer customer2 = new Customer("name2", "email2", 22);
        customerRepository.save(customer1);
        // then
        Assert.assertFalse(customerRepository.delete(customer2));
        Assert.assertEquals(1, customerRepository.findAll().size());
    }

    @Test
    public void test_get_one_customer_from_repository() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer1 = new Customer("name1", "email1", 22);
        Customer customer2 = new Customer("name2", "email2", 22);
        customerRepository.save(customer1);
        customerRepository.save(customer2);
        // then
        Assert.assertEquals(2, customerRepository.findAll().size());

        Assert.assertEquals(customer1, customerRepository.getOne(1));
        Assert.assertEquals(customer2, customerRepository.getOne(2));

        Assert.assertNotEquals(customer2, customerRepository.getOne(1));
        Assert.assertNotEquals(customer1, customerRepository.getOne(2));

        Assert.assertNull(customerRepository.getOne(33));
    }

    @Test
    public void test_delete_all_customers() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer1 = new Customer("name1", "email1", 22);
        Customer customer2 = new Customer("name2", "email2", 22);
        Customer customer3 = new Customer("name3", "email3", 22);
        customerRepository.save(customer1);
        customerRepository.save(customer2);
        customerRepository.save(customer3);
        // then
        Assert.assertEquals(3, customerRepository.findAll().size());

        Customer[] customers = {customer1, customer2};
        customerRepository.deleteAll(Arrays.asList(customers));

        Assert.assertEquals(1, customerRepository.findAll().size());
        Assert.assertEquals(customer3, customerRepository.getOne(3));
    }

    @Test
    public void test_save_all_customers() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer1 = new Customer("name1", "email1", 22);
        Customer customer2 = new Customer("name2", "email2", 22);
        Customer customer3 = new Customer("name3", "email3", 22);
        Customer[] customers = {customer1, customer2, customer3};
        customerRepository.saveAll(Arrays.asList(customers));
        // then
        Assert.assertEquals(3, customerRepository.findAll().size());
    }

    @Test
    public void test_save_the_same_customer_twice() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        // when
        Customer customer = new Customer("name1", "email1", 22);
        Customer[] customers = {customer, customer};
        customerRepository.saveAll(Arrays.asList(customers));
        // then
        Assert.assertEquals(1, customerRepository.findAll().size());
    }

    @Test
    public void test_update_customer() {
        // given
        CustomerRepository customerRepository = new CustomerRepository();
        Customer customer = new Customer("name1", "email1", 22);
        customerRepository.save(customer);

        // when
        Customer customerCandidate = customerRepository.getOne(1);
        customerCandidate.setAge(10);
        customerRepository.updateById(1, customerCandidate);

        // then
        Assert.assertEquals(1, customerRepository.findAll().size());
        Assert.assertEquals(10, customerRepository.getOne(1).getAge());
    }

    @Test
    public void test_delete_accounts_by_customer() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        CustomerRepository customerRepository = new CustomerRepository();

        Customer customer = new Customer("name1", "email1", 22);
        Account account = new Account(Currency.EUR, customer);
        accountRepository.save(account);
        customer.addAccount(account);
        customerRepository.save(customer);

        // when
        accountRepository.deleteAccountsByCustomer(customer);

        // then
        Assert.assertEquals(0, accountRepository.findAll().size());
    }
}
