package com.app.repository;

import com.app.model.Account;
import com.app.model.Currency;
import com.app.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class AccountRepositoryTest {

    @Test
    public void test_adding_new_account() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        accountRepository.save(new Account(Currency.EUR, new Customer("name", "email", 22)));
        // then
        Assert.assertEquals(1, accountRepository.findAll().size());
    }

    @Test
    public void test_removing_account() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account = new Account(Currency.EUR, new Customer("name", "email", 22));
        accountRepository.save(account);
        // then
        Assert.assertTrue(accountRepository.delete(account));
        Assert.assertEquals(0, accountRepository.findAll().size());
    }

    @Test
    public void test_removing_account_by_id() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account = new Account(Currency.EUR, new Customer("name", "email", 22));
        accountRepository.save(account);
        // then
        Assert.assertFalse(accountRepository.deleteById(999));
        Assert.assertTrue(accountRepository.deleteById(1));
        Assert.assertEquals(0, accountRepository.findAll().size());
    }

    @Test
    public void test_removing_account_by_non_existing_id() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account = new Account(Currency.EUR, new Customer("name", "email", 22));
        accountRepository.save(account);
        // then
        Assert.assertFalse(accountRepository.deleteById(999));
        Assert.assertEquals(1, accountRepository.findAll().size());
    }

    @Test
    public void test_removing_account_by_non_existing_account() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account1 = new Account(Currency.EUR, new Customer("name", "email", 22));
        Account account2 = new Account(Currency.UAH, new Customer("name", "email", 22));
        accountRepository.save(account1);
        // then
        Assert.assertFalse(accountRepository.delete(account2));
        Assert.assertEquals(1, accountRepository.findAll().size());
    }

    @Test
    public void test_get_one_account_from_repository() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account1 = new Account(Currency.EUR, new Customer("name", "email", 22));
        Account account2 = new Account(Currency.UAH, new Customer("name", "email", 22));
        accountRepository.save(account1);
        accountRepository.save(account2);
        // then
        Assert.assertEquals(2, accountRepository.findAll().size());

        Assert.assertEquals(account1, accountRepository.getOne(1));
        Assert.assertEquals(account2, accountRepository.getOne(2));

        Assert.assertNotEquals(account2, accountRepository.getOne(1));
        Assert.assertNotEquals(account1, accountRepository.getOne(2));

        Assert.assertNull(accountRepository.getOne(33));
    }

    @Test
    public void test_delete_all_accounts() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account1 = new Account(Currency.EUR, new Customer("name", "email", 22));
        Account account2 = new Account(Currency.UAH, new Customer("name", "email", 22));
        Account account3 = new Account(Currency.CHF, new Customer("name", "email", 22));
        accountRepository.save(account1);
        accountRepository.save(account2);
        accountRepository.save(account3);
        // then
        Assert.assertEquals(3, accountRepository.findAll().size());

        Account[] accounts = {account1, account2};
        accountRepository.deleteAll(Arrays.asList(accounts));

        Assert.assertEquals(1, accountRepository.findAll().size());
        Assert.assertEquals(account3, accountRepository.getOne(3));
    }

    @Test
    public void test_save_all_accounts() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account1 = new Account(Currency.EUR, new Customer("name", "email", 22));
        Account account2 = new Account(Currency.UAH, new Customer("name", "email", 22));
        Account account3 = new Account(Currency.CHF, new Customer("name", "email", 22));
        Account[] accounts = {account1, account2, account3};
        accountRepository.saveAll(Arrays.asList(accounts));
        // then
        Assert.assertEquals(3, accountRepository.findAll().size());
    }

    @Test
    public void test_save_the_same_account_twice() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account = new Account(Currency.EUR, new Customer("name", "email", 22));
        Account[] accounts = {account, account};
        accountRepository.saveAll(Arrays.asList(accounts));
        // then
        Assert.assertEquals(1, accountRepository.findAll().size());
    }

    @Test
    public void test_save_the_same_account_twice_by_calling_save_method() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        // when
        Account account = new Account(Currency.EUR, new Customer("name", "email", 22));
        accountRepository.save(account);
        accountRepository.save(account);
        // then
        Assert.assertEquals(1, accountRepository.findAll().size());
    }

    @Test
    public void test_update_account() {
        // given
        AccountRepository accountRepository = new AccountRepository();
        Account account = new Account(Currency.EUR, new Customer("name", "email", 22));
        accountRepository.save(account);

        // when
        Account accountCandidate = accountRepository.getOneByNumber(account.getNumber());
        accountCandidate.setBalance(33.4);
        accountRepository.updateById(1, accountCandidate);

        // then
        Assert.assertEquals(1, accountRepository.findAll().size());
        Assert.assertEquals(33.4, accountRepository.getOne(1).getBalance(), 0.0);
    }
}
